#include <Arduino.h>
#include <Wire.h>


#define CLOCKIN 2

struct divisionData {
    byte pin;
    byte division;
    bool random;
};

/* if random is enabled it will be changing the division on each
   clockIn with a "random" value between 0 and the division value

   a good page to determine the least common multiple ~ when all your divisions trigger at once: https://www.matheretter.de/rechner/kgv
   your limit is the limit of the datatype, defined in CLOCKCOUNTER_MAX

   for example:
    2, 4, 8, 16, 32, 64 = 64(boring)
    2, 4, 5, 8, 12, 16 = 240
    2, 5, 6, 7, 13, 16 = 21840
    3, 5, 7, 11, 13, 19 = 285285

    the last example results in a time of ~29 hours at 161 bpm(285285/161/60) for a full cycle

    if you enable CRAZY_MODE all outputs get a new random division each cycle


   change divisions here and enable randomness
   pin division random*/
divisionData divisions[] = {
    { 3, 2,  false },
    { 4, 4,  false },
    { 5, 6,  false },
    { 6, 8,  false },
    { 7, 12, false },
    { 8, 16, false }
};



//0:no debug messages; 1:only prints when new divisions are set, CRAZY_MODE have to be enabled; 2: print all output states for each cycle
#define DEBUG_LEVEL 0
#define CLOCKCOUNTER_MAX 2147483647
#define CRAZY_MODE true
#define CRAZY_MODE_MIN 1
#define CRAZY_MODE_MAX 16
long clockCounter = 0;
bool triggered = false;
long previous;

void clockHandler() {
    clockCounter++;
    bool all = true;
    for (divisionData div : divisions) {
        // trigger this output if division is divisible by the division value
        // randomize division if random is true
        digitalWrite(div.pin, clockCounter % ((div.random) ? random(0, div.division) : div.division) == 0);

        if (DEBUG_LEVEL > 1) {
            if (digitalRead(div.pin))
                Serial.print("X");
            else
                Serial.print(" ");
            Serial.print(" | ");
        }

        if(CRAZY_MODE && !digitalRead(div.pin))
            all = false;
    }
    if (DEBUG_LEVEL > 1)
        Serial.println("(" + String(clockCounter) + ")");
    // reset counter
    if (clockCounter == CLOCKCOUNTER_MAX)
        clockCounter = 0;
    //all outputs where triggered in this cycle -> new divisions for all outputs
    if (CRAZY_MODE && all) {
        if (DEBUG_LEVEL > 0) {
            Serial.println(clockCounter);
            Serial.println("NEW DIVISIONS");
            Serial.println("#############");
        }
        for (divisionData& div : divisions) {
            div.division = random(CRAZY_MODE_MIN, CRAZY_MODE_MAX);
            if (DEBUG_LEVEL > 0)
                Serial.print(String(div.division) + " | ");
        }
        if (DEBUG_LEVEL > 0) {
            Serial.println("");
            Serial.println("#############");
        }
    }
}

void bootAnimation() {
    // displays 1312 at startup
    digitalWrite(divisions[0].pin, HIGH);
    delay(1000);
    digitalWrite(divisions[0].pin, HIGH);
    digitalWrite(divisions[1].pin, HIGH);
    digitalWrite(divisions[2].pin, HIGH);
    delay(1000);
    digitalWrite(divisions[0].pin, HIGH);
    digitalWrite(divisions[1].pin, LOW);
    digitalWrite(divisions[2].pin, LOW);
    delay(1000);
    digitalWrite(divisions[0].pin, HIGH);
    digitalWrite(divisions[1].pin, HIGH);
    delay(1000);
}


void setup() {
    pinMode(CLOCKIN, INPUT);
    for (divisionData div : divisions)
        pinMode(div.pin, OUTPUT);
    if (DEBUG_LEVEL > 0)
        Serial.begin(9600);
    bootAnimation();
}

void loop() {
    if (DEBUG_LEVEL > 0) {
        clockHandler();
    } else {
        if (!triggered && digitalRead(CLOCKIN) == HIGH) {
            clockHandler();
            triggered = true;
            previous = millis();
        }
        if (millis() - previous >= 50) {
            // lower all pins
            for (divisionData div : divisions) {
                digitalWrite(div.pin, LOW);
            }
        }
        if (digitalRead(CLOCKIN) == LOW)
            triggered = false;
    }
}
